<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
    // return redirect(route('home'));
});

Route::get('/api', function () {
    return view('api');
    // return redirect(route('home'));
});



Route::resource('todos','TodoController');
Route::resource('notes','NoteController');
Route::resource('apinotes','ApinoteController');
Route::resource('apitodos','ApitodoController');