<?php

namespace App\Http\Controllers;
use App\Todo;
use App\Todo_status;

use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index(){

    	$todos = Todo::paginate(9);

    	return view('todos.index')
    	->with('todos', $todos);

    	

    }

    public function store (Request $request){

    }

   public function create(){

   } 

   public function show($id){

   }


   public function update(Request $request, $id){

   	echo $request->input('status');

   }

   public function edit($id){

   	$todo = Todo::find($id);

   	return view('todos.edit')
   		->with('todo',$todo);

   }

   public function destroy($id){
   	
   }
}
