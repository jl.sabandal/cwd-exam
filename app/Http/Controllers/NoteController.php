<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;



class NoteController extends Controller
{
    public function index(){

    	$notes = Note::paginate(10);

    			
    	return view('notes.index')
    		->with('notes',$notes);


    }

    public function store (Request $request){

    }

   public function create(){

   } 

   public function show($id){

   	$note = Note::find($id);


   	return view('notes.show')
   		->with('note',$note);

   }


   public function update(Request $request, $id){

   }

   public function edit($id){

   }

   public function destroy($id){
   	
   }
}
