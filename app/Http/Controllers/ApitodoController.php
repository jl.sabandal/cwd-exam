<?php

namespace App\Http\Controllers;
use App\Todo;
use DB;
use App\Todo_status;

use Illuminate\Http\Request;


class ApitodoController extends Controller
{
    public function index(){

    	// $notes = Todo::paginate(10);
    	$todos = DB::table('todos')
    		->orderBy("id", "desc")
    		->paginate(9);



    	$status = Todo_status::All();

    		return view('apitodos.index')
    			->with('todos', $todos)
    			->with('status',$status);

    

    }

    public function store (Request $request){

    	$request->validate([
            'name' => 'required|string',
        ]);

        $name = $request->input('name');
        $todo = new Todo;
        $todo->name = $name;
        $todo->todo_status = 2;
        $todo->save();

        return redirect( route('apitodos.index'));  
    }

   public function create(){

   } 

   public function show($id){

   }


   public function update(Request $request, $id){

   		$todo = Todo::find($id);
   		$todo->name = $request->name;
   		$todo->todo_status = $request->status;
   		$todo->save();

   		return redirect( route('apitodos.index'));

   }

   public function edit($id){

   		$todo = Todo::find($id);
   		$todo_status = Todo_status::All();

   		return view('apitodos.edit')
   			->with('todo', $todo)
   			->with('todo_status', $todo_status);


   }

   public function destroy($id){

   	Todo::destroy($id);
   
   	return redirect( route('apitodos.index'));



   	
   }
}
