<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use DB;

class ApinoteController extends Controller
{
    public function index(){

    	$notes = DB::table('notes')
    		->orderBy("id", "desc")
    		->paginate(9);


    		return view('apinotes.index')
    			->with('notes', $notes);


    }

    public function store (Request $request){
    	$request->validate([
            'title' => 'required|string',
            'content' => 'required|string'
        ]);

        $title = $request->input('title');
        $content = $request->input('content');


        $note = new Note;
        $note->title = $title;
        $note->content = $content;
        $note->save();

        return redirect( route('apinotes.index'));  
    }

   public function create(){

   } 

   public function show($id){

   }


   public function update(Request $request, $id){

   		$note = Note::find($id);
   		$note->title = $request->title;
   		$note->content = $request->content;
   		$note->save();

   		return redirect( route('apinotes.index'));




   }

   public function edit($id){

   		$note = Note::find($id);

   		return view('apinotes.edit')
   			->with('note', $note);

   }

   public function destroy($id){

   	Note::destroy($id);
   
   	return redirect( route('apinotes.index'));
   	
   }
}
