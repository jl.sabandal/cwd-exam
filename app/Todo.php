<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    // use softDeletes;

    public function todo_status(){
    	return $this->belongsTo('App\Todo_status');
    }
}
