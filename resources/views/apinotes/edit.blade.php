@extends('layouts.app')


@section('title')
Update Notes
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-12 col-md-4 mx-auto my-5 ">

				<div class="card shadow-lg border-0">
				  <div class="card-header text-light text-center col-color1">
				    <h2>Update Note</h2>
				  </div>
				  <div class="card-body col-color2">
				  <form action="/apinotes/{{$note->id}}" method="POST">
						@csrf
				   		@method('PUT')
				  	
				  	<div class="form-group">
					    <label for="">Title</label>
					    <input type="text" class="form-control" id="title" name="title" value="{{$note->title}}" required>
					</div>

					<div class="form-group">
					    <label for="">Content</label>
					    <textarea class="form-control"  rows="3" required name="content" id="content">{{$note->content}}</textarea>
					 </div>

					 <input type="submit" value="Save" class="btn btn-success ">
					 <a href="{{route('apinotes.index')}}"	class="btn btn-danger ">Cancel</a>


				  </form>
				</div>
				   <div class="card-footer text-muted col-color1">
				  
				  </div>
				</div>

			</div>

			
		</div>
	</div>
@endsection