@extends('layouts.app')


@section('title')
Notes Dashboard
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-12 col-md-3 mx-auto my-5 ">

				<div class="card shadow-lg border-0">
				  <div class="card-header text-light text-center col-color1">
				    <h2>Create new Note</h2>
				  </div>
				  <div class="card-body col-color2">
				  <form action="{{route('apinotes.store')}}" method="POST">
						@csrf
				  	
				  	<div class="form-group">
					    <label for="">Title</label>
					    <input type="text" class="form-control" id="title" name="title" required>
					</div>

					<div class="form-group">
					    <label for="">Content</label>
					    <textarea class="form-control"  rows="3" required name="content" id="content"></textarea>
					 </div>

					 <input type="submit" value="Save" class="btn btn-success form-control">


				  </form>
				</div>
				   <div class="card-footer text-muted col-color1">
				  
				  </div>
				</div>

			</div>

			<div class="col-12 col-md-9 mx-auto my-5 ">
				<div class="card shadow-lg border-0	">
				  <div class="card-header text-light text-center col-color1">
				    <h2>Note Lists</h2>
				  </div>
				  <div class="card-body col-color2">
				  	<div class="table-responsive-sm table-sm bg-white table-striped">
				   		<table class="table">
				   			<thead>
				   				<tr>
				   					<th>Id</th>
				   					<th>Title</th>
				   					<th>Content</th>
				   					<th colspan="2" class="text-center">Action</th>
				   				</tr>
				   			</thead>
				   			<tbody>
				   				@foreach($notes as $note)
				   				<tr>
				   					<td>{{$note->id}}</td>
				   					<td>{{$note->title}}</td>
				   					<td>{{$note->content}}</td>
				   					<td class="text-right">
				   						<a href="/apinotes/{{$note->id}}/edit" class="btn btn-success btn-sm">Edit</a>
				   					</td>
				   					<td class="text-right">
				   						<form action="/apinotes/{{$note->id}}" method="POST">
				   							@csrf
				   							@method('DELETE')
				   							<input type="submit" name="submit" value="delete" class="btn btn-danger btn-sm">
				   						</form>
				   					</td>
				   				</tr>
				   				@endforeach
				   			</tbody>
				   			
				   		</table>
				  </div>
				</div>
				   <div class="card-footer text-muted col-color1">
				   		{{$notes->links()}} 
				  </div>
				</div>
			</div>

			
		</div>
	</div>
@endsection