@extends('layouts.app')


@section('title')
Note Taking Activity
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 col-color1">
				<div class="card text-center mt-5 mb-5">
				  <div class="card-header col-color2">
				   <h1 class="text-left">Notes</h1>
				  </div>
				  <div class="card-body">
				    <ul class="list-group list-group-flush">

				    	@foreach($notes as $note)
		 				<li class="list-group-item">
		 					<a href="/notes/{{$note->id}}" class="anchor">	 							
		 							{{$note->title}}
		 					</a>
		 				</li>
		 				@endforeach
					</ul>
				    
				  </div>
				  <div class="card-footer text-muted col-color2	">
				    {{$notes->links()}}
				  </div>
				</div>			  	
			</div>

			<div class="col-12 col-md-6 col-color2 ">
				<div class="jumbotron jumbotron-fluid mt-5 mb-5 col-color1 shadow text-light">
				  <div class="container">
				    <h1 class="display-4">Note - Taking Activity</h1>
				    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				  </div>
				</div>

				<div class="jumbotron jumbotron-fluid bg-transparent shadow-lg mb-5">
				  <div class="container ">
				    
				    <h1 class="display-4">To Do Tracking Activity</h1>
				   <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				    <a href="{{route('todos.index')}}" class="btn btn-primary">View Page</a>
				  </div>
				</div>
			</div>

		</div>
	</div>
@endsection