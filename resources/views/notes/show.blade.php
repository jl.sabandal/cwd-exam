@extends('layouts.app')


@section('title')
Note Taking Activity
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 col-color1">
				<div class="card mt-5 mb-5">
				  <div class="card-header col-color2">
				   <h1>Notes</h1>
				  </div>
				  <div class="card-body">
				    <ul class="list-group list-group-flush text-center">

				    	
		 				<li class="list-group-item">			
		 						<h2>{{$note->title}}</h2>
		 				</li>
		 				<li>			
		 					<p class="m-5">	
		 						<h3>		 							
		 							{{$note->content}}
		 						</h3>
		 					</p>
		 				</li>
		 			
					</ul>
				    
				  </div>
				  <div class="card-footer text-muted col-color2	text-right">
						<a href="{{route('notes.index')}}" class="btn btn-primary">Back</a>
				  </div>
				</div>			  	
			</div>

			<div class="col-12 col-md-6 col-color2 ">
				<div class="jumbotron jumbotron-fluid mt-5 mb-5 col-color1 text-light">
				  <div class="container">
				    <h1 class="display-4">Note - Taking Activity</h1>
				    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				  </div>
				</div>

				<div class="jumbotron jumbotron-fluid bg-transparent shadow-lg mb-5">
				  <div class="container">
				    
				    <h1 class="display-4">To Do Tracking Activity</h1>
				   <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				    <a href="{{route('todos.index')}}" class="btn btn-primary">View Page</a>
				  </div>
				</div>
			</div>

		</div>
	</div>
@endsection