@extends('layouts.app')


@section('title')
To Do
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-5 mx-auto mt-5">
				<div class="card shadow-lg">
				  <div class="card-header bg-info text-center">
				    <h1>TO DO LISTS</h1>
				  </div>
				  <div class="card-body">
				    <form action="/todos/{{$todo->id}}" method="POST">
				    	@csrf
						@method('PUT')

				    	{{$todo->name}}
				    	{{$todo->todo_status}}
		
					
						<input type="checkbox" class="checkBox text-right" name="status" id="status" value="true">
				    	

				    	<input type="submit" value="submit">
				    


						{{-- <div class="form-group">
						    <label for="exampleFormControlTitle">Done</label>
						   	<input type="checkbox" value="false" checked="checked" class="checkBox text-right">
						</div> --}}

				    </form>
				  </div>
				</div>
			</div>
		</div>
	</div>
@endsection