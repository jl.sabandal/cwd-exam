@extends('layouts.app')


@section('title')
To Do Activity
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 col-color2">
				<div class="card mt-5 mb-5 border-0">
				  <div class="card-header col-color1">
				   <h1 class="text-left text-white">To Do List</h1>
				  </div>
				  <div class="card-body">
				   <table class="table table-hover table-striped table-sm">
				    	<thead>
				    	</thead>
				    	<tbody>
				    		@foreach($todos as $todo)
				    		<tr>
				    			<th scope="row">
				    				<h3 class="anchor text-muted">				    					
				    					{{$todo->name}}
				    				</h3>
				    	
				    			</th>
								
								@if($todo->todo_status != 1)
									<td class="text-right"><input type="checkbox" value="false" class="checkBox" disabled></td>
									@else
										<td class="text-right"><input type="checkbox" value="true" checked="checked" class="checkBox" disabled></td>
										@endif
				    		</tr>
				    		@endforeach
				    	</tbody>
				    </table>
				    
				  </div>
				  <div class="card-footer text-muted col-color1	">
						{{$todos->links()}}
				  </div>
				</div>			  	
			</div>

			<div class="col-12 col-md-6 col-color1 ">
				<div class="jumbotron jumbotron-fluid mt-5 mb-5 col-color2 shadow-lg">
				  <div class="container">
				  	<h1 class="display-4">To Do Tracking Activity</h1>
				   <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				    
				  </div>
				</div>

				<div class="jumbotron jumbotron-fluid bg-transparent shadow mb-5">
				  <div class="container text-light">
				    
				    <h1 class="display-4">Note - Taking Activity</h1>
				    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus voluptate facili</p>
				    <a href="{{route('notes.index')}}" class="btn btn-primary">View Page</a>
				  </div>
				</div>
			</div>

		</div>
	</div>
@endsection