@extends('layouts.app')


@section('title')
To Do Dashboard
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 mx-auto my-5 ">
				<div class="card shadow-lg border-0">
				  <div class="card-header text-light  col-color1">
				    <h1>UPDATE DATA</h1>
					
				  </div>
				  <div class="card-body col-color2">
				   		<form action="/apitodos/{{$todo->id}}" method="POST">
				   			@csrf
				   			@method('PUT')
							<div class="row">
							    <div class="col-12 col-md-9">
							      <input type="text" class="form-control" name="name" id="name" value="{{$todo->name}}" required>
							    </div>
							    <div class="col-12 col-md-3">
							      <select class="custom-select mr-sm-2" id="status" name="status">
							        	@foreach($todo_status as $todo_status)
                                            <option value="{{$todo_status->id}}" @if($todo->todo_status == $todo_status->id)selected @endif>{{$todo_status->name}}</option>
                                        @endforeach
							      </select>
							    </div>
							</div>
							<input type="submit" value="Update" class="btn btn-success my-3">
							<a href="{{route('apitodos.index')}}" class="btn btn-danger">Cancel</a>


				   		</form>


				  </div>
				   <div class="card-footer text-muted col-color1">
				   	
				  </div>
				</div>
			</div>
		</div>
	</div>
@endsection