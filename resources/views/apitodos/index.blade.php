@extends('layouts.app')


@section('title')
To Do Dashboard
@endsection


@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 mx-auto my-5 ">
				<div class="card shadow-lg border-0">
				  <div class="card-header text-light text-center col-color1">
				    <h1>TO DO LIST</h1>
					<form action="{{route('apitodos.store')}}" method="POST">
						@csrf
						<div class="row">
						    <div class="col-12 col-md-9">
						      <input type="text" class="form-control" name="name" id="name" placeholder="Title ..." required>
						    </div>
						    <div class="col-12 col-md-3">
						      <input type="submit" class="form-control btn btn-success" value="Add">
						    </div>
						  </div>
					</form>

				  </div>
				  <div class="card-body col-color2">
				  	<div class="table-responsive-sm bg-white table-sm table-striped">
				   		<table class="table">
				   			<thead>
				   				<tr>
				   					<th>Id</th>
				   					<th>Name</th>
				   					<th>Status</th>
				   					<th colspan="2" class="text-center">Action</th>
				   				</tr>
				   			</thead>
				   			<tbody>
				   				@foreach($todos as $todo)
				   				<tr>
				   					<td>{{$todo->id}}</td>
				   					<td>{{$todo->name}}</td>
				   					<td>{{$status[$todo->todo_status - 1]->name}}</td>
				   					<td class="text-right">
				   						<a href="/apitodos/{{$todo->id}}/edit" class="btn btn-success btn-sm">Edit</a>
				   					</td>
				   					<td class="text-right">
				   						<form action="/apitodos/{{$todo->id}}" method="POST">
				   							@csrf
				   							@method('DELETE')
				   							<input type="submit" name="submit" value="delete" class="btn btn-danger btn-sm">
				   						</form>
				   					</td>
				   				</tr>
				   				@endforeach
				   			</tbody>
				   			
				   		</table>
				  </div>
				</div>
				   <div class="card-footer text-muted col-color1">
				   		{{$todos->links()}} 
				  </div>
				</div>
			</div>
		</div>
	</div>
@endsection