@extends('layouts.app')


@section('title')
Api's 
@endsection


@section('content')
	<div class="container mt-5">
		<div class="row">
			<div class="col-12 col-md-6 col-color2 mt-5">
				<div class="jumbotron jumbotron p-5 mt-5  col-color1 shadow-lg">
				  <div class="container text-light">
				    
				    <h1 class="display-4">Manage</h1>
				    <h1 class="display-4">Note - Taking Activity</h1>
				   
				    <a href="{{route('apinotes.index')}}" class="btn btn-primary">View Page</a>
				  </div>
				</div>		  	
			</div>

			<div class="col-12 col-md-6 col-color1 mt-5 ">
				<div class="jumbotron jumbotron p-5 mt-5 mb-5 col-color2 shadow-lg">
				  <div class="container">
				  	<h1 class="display-4">Manage</h1>
				  	<h1 class="display-4">To Do Tracking Activity</h1>
				    <a href="{{route('apitodos.index')}}" class="btn btn-primary">View Page</a>
				  </div>
				</div>

				
			</div>

		</div>
	</div>
@endsection