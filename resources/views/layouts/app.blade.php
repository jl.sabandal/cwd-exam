<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title')</title>

		{{-- bootrap --}}
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 

    	{{-- {{bootstap external}} --}}
	    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">

	    <!-- Styles -->
	    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

	</head>
	<body>
		<ul class="nav justify-content-end bg-nav">
		  <li class="nav-item">
		    <a class="nav-link active text-white font-weight-bold" href="/">Home</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-white font-weight-bold" href="/api">API's</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled text-white" href="#" tabindex="-1" aria-disabled="true">CWD-EXAM</a>
		  </li>
		</ul>
		
		<main>
			@yield('content')			
		</main>



	<script src="{{asset('js/app.js')}}"></script>
	<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
	<script src="{{asset('js/all.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>    
	<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>    
	<script src="{{asset('js/bootstrap.bundle.js')}}"></script>    

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

	</body>
</html>