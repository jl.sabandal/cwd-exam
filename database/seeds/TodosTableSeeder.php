<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
        	'name' => 'Hit the gym',
        	'todo_status' => 2
        ]);
        DB::table('todos')->insert([
        	'name' => 'Pay bills',
        	'todo_status' => 1
        ]);
        DB::table('todos')->insert([
        	'name' => 'Clean the car',
        	'todo_status' => 1
        ]);
        DB::table('todos')->insert([
        	'name' => 'Buy groceries',
        	'todo_status' => 2
        ]);
        DB::table('todos')->insert([
        	'name' => 'Read a book',
        	'todo_status' => 1
        ]);
        DB::table('todos')->insert([
        	'name' => 'Organize home',
        	'todo_status' => 2
        ]);
        DB::table('todos')->insert([
        	'name' => 'Play Basketball',
        	'todo_status' => 1
        ]);
    }
}
