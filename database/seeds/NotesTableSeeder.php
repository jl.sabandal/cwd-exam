<?php

use Illuminate\Database\Seeder;

class NotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 11; $i++){
	        DB::table('notes')->insert([
	        	'title' => 'Title '.$i,
	        	'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In aliquam delectus ut veniam fuga blanditiis adipisci'
	        ]);	
    	}

        
    }
}
