<?php

use Illuminate\Database\Seeder;

class TodoStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todo_statuses')->insert([
        	'name' => 'done'
        ]);
        DB::table('todo_statuses')->insert([
        	'name' => 'pending'
        ]);
    }
}
